using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] float jumpForce, minGroundedDistance;
    [SerializeField] Transform foot;
    public VariableJoystick variableJoystick;
    public Rigidbody rb;


    private void Start()
    {

        GameEventHandler.Instance.OnClickJump += jump;
    }

    public void FixedUpdate()
    {
        Vector3 direction = Vector3.forward * variableJoystick.Vertical + Vector3.right * variableJoystick.Horizontal;
        rb.AddForce(direction * speed * Time.fixedDeltaTime, ForceMode.VelocityChange);

#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Space))
        {
            jump();
        }
#endif
    }

    void jump()
    {
        
        if (groundDistance()<minGroundedDistance)
            rb.AddForce(Vector3.up *jumpForce* Time.fixedDeltaTime, ForceMode.Impulse);
    }

    float groundDistance()
    {
        float _dist = 999;
        RaycastHit hit;
        if (Physics.Raycast(foot.position, transform.TransformDirection(Vector3.down), out hit))
        {
            if(hit.collider.tag=="Ground")
                _dist = Vector3.Distance(hit.point, foot.position);
        }
        return _dist;
    }
}
