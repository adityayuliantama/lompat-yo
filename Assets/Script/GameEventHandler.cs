using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameEventHandler : MonoBehaviour
{
    public static GameEventHandler Instance;

    public event Action OnClickJump;
    public event Action<int> OnRopeJumped;
    // Start is called before the first frame update
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public void JumpClicked()
    {
        if (OnClickJump != null)
        {
            OnClickJump();
        }
    }

    public void RopeJumped(int p_ropeCount)
    {
        Debug.Log("jupmed "+p_ropeCount+" rope");
        if (OnRopeJumped != null)
        {
            OnRopeJumped(p_ropeCount);
        }
    }
}
