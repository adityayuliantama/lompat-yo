using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopeObject : MonoBehaviour
{
    float speed = 10.0f;
    float lifeTime = 50;

    public bool isReady;

    [SerializeField]
    MeshRenderer renderer;

    [SerializeField] Collider collider;

    RopeManager manager;

    private void Start()
    {

    }

    public void Initialize(float p_speed, float p_lifeTime, RopeManager p_manager)
    {
        manager = p_manager;
        speed = p_speed;
        lifeTime = p_lifeTime;
        isReady = false;
        collider.enabled = false;
        StartCoroutine(ie_initializing());
    }
    private void Update()
    {
        if (isReady)
        {
            transform.Rotate(speed * Time.deltaTime*Vector3.forward);
            if (lifeTime > 0)
            {
                lifeTime -= Time.deltaTime;
                Debug.Log(lifeTime);
            }
            else
            {
                manager.ReleaseRope(this);
            }
        }
        else
        {

        }
    }
    IEnumerator ie_initializing()
    {
        float initializeTime = 4;
        float blinkTime = initializeTime * 0.85f;
        bool isShowing = true;

        renderer.material.color = new Color(1, 1, 1, 0.5f);

        while (initializeTime > 0)
        {
            initializeTime -= Time.deltaTime;

            if (blinkTime > initializeTime)
            {
                blinkTime = initializeTime * 0.85f;
                renderer.enabled = isShowing;
                isShowing = !isShowing;
            }

            yield return null;
        }
        renderer.enabled = true;

        renderer.material.color = new Color(1, 1, 1, 1f);
        isReady = true;
        collider.enabled = true;
    }
}
