using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRopeChecker : MonoBehaviour
{
    [SerializeField] float minGroundedDistance;
    [SerializeField] Transform foot;
    List<RopeObject> ropeJumped = new List<RopeObject>();

    private void Update()
    {
        ropeCheck();
    }
    void ropeCheck()
    {

        RaycastHit hit;

        float _dist = 999;
        if (Physics.Raycast(foot.position, transform.TransformDirection(Vector3.down), out hit))
        {
            if (hit.collider.tag == "Rope")
            {
                RopeObject _rope = hit.collider.gameObject.GetComponent<RopeObject>();
                if (_rope != null && !ropeJumped.Contains(_rope) && _rope.isReady)
                {
                    ropeJumped.Add(_rope);
                }
            } 
            else if (hit.collider.tag == "Ground")
            {
                _dist = Vector3.Distance(hit.point, foot.position);
                if (_dist < minGroundedDistance)
                {
                    if (ropeJumped.Count > 0)
                    {

                        GameEventHandler.Instance.RopeJumped(ropeJumped.Count);
                    }
                    ropeJumped.Clear();
                    _dist = Vector3.Distance(hit.point, foot.position);
                }
            }
        }
    }
}
