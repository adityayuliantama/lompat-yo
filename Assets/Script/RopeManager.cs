using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class RopeManager : MonoBehaviour
{
    ObjectPool<RopeObject> ropePool;
    [SerializeField] RopeObject ropePrefab;


    [SerializeField] float ropeSpeed = 15, ropeLifeTime = 200, spawnRadius = 3, spawnDelay=10;
    private void Start()
    {
        ropePool = new ObjectPool<RopeObject>(
            () => {return Instantiate(ropePrefab, transform); },
            item => { gameObject.SetActive(true); },
            item => { gameObject.SetActive(false); },
            item => { Destroy(item); },
            false, 10,10
            )
            ;
        StartCoroutine(ie_spawning());
    }

    IEnumerator ie_spawning()
    {
        while (true)
        {
            spawnRope();
            yield return new WaitForSeconds(spawnDelay);
        }

    }

    void spawnRope()
    {
        Vector3 _spawnPos = transform.position + (Random.Range(-spawnRadius, spawnRadius)*Vector3.forward)+ (Random.Range(-spawnRadius, spawnRadius) * Vector3.right);
        RopeObject _rope = ropePool.Get();
        _rope.transform.position = _spawnPos;
        _rope.Initialize(ropeSpeed*(Random.Range(-1,0)==-1?-1:1),30,this);
    }

    public void ReleaseRope(RopeObject p_rope)
    {
        ropePool.Release(p_rope);
    }
}
